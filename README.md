# r_TD_Rmarkdown

*Réalisation d'un rapport RMarkdown à la suite d'un travail d'exploitation de la librairie ggplot2*

## Objectif du travail 

* Utilisation du *knitr* package sur RStudio
* Avec présentation du code ou non/mise en cache/chargements de librairie en arrière plan
* Incorportation des graphes exploités lors d'un précédent TD
* Exportation en format HTML 

## Pour aller plus loin

* Exploration des cheatsheet Markdown/Rmarkdown
* Exploitation d'un CSS pour une personnalisation de la présentation

------------------------------------------------------
